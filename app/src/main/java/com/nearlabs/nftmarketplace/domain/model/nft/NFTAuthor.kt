package com.nearlabs.nftmarketplace.domain.model.nft

data class NFTAuthor(
    val name: String,
    val image: String
)
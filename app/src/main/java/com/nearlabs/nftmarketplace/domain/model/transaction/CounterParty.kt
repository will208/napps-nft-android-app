package com.nearlabs.nftmarketplace.domain.model.transaction

data class CounterParty(
    val walletId: String?,
    val name: String?
)
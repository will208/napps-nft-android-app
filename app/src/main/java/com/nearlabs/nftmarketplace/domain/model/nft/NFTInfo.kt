package com.nearlabs.nftmarketplace.domain.model.nft

data class NFTInfo(
    val tokenId: String,
    val contract: String
)
package com.nearlabs.nftmarketplace.domain.model.settings

data class Security(
    val twoFAAuthentication: Boolean
)
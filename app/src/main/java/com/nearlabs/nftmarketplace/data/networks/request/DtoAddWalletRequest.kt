package com.nearlabs.nftmarketplace.data.networks.request

data class DtoAddWalletRequest(
    val name: String
)